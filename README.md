Privacy Policy

This page explains the privacy policy of JoinPics "JoinPics Collage Maker Image Editor Text on Photos" app. 
This page is used to inform users regarding JoinPics policies with the collection, use, and disclosure of personal information or device information if anyone decided to use JoinPics.


Following are the main points:

1. JoinPics does not collect any personal information about you. 
2. JoinPics does not share your personal information with anyone.
3. JoinPics needs camera and storage permissions on your device, to take images and store them to your device. JoinPics does not automatically share your photos with anyone.
4. JoinPics does not store any cookies.
5. JoinPics does not maintain any logs having your device information or your personal information.
6. If you have any questions or suggestions about this Privacy Policy, do not hesitate to contact me at memezombies@gmail.com.
7. JoinPics takes your privacy seriously.
